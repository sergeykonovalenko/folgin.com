$(document).ready(function () {
    'use strict';

    const is_mobile = isMobile();

    // projects — remove animation delay on mobile
    if (is_mobile) {
        $('body').addClass('is-mobile');
        $('.project').attr('data-wow-delay', 0);
    }

    // wow
    new WOW().init();

    // parallax
    if (!is_mobile) {
        let $window = $(window);

        $('[data-type="background"]').each(function() {
            let $bgobj = $(this);

            $(window).scroll(function() {
                let yPos = -($window.scrollTop() / $bgobj.data('speed'));
                let coords = '50% '+ yPos + 'px';

                $bgobj.css({ backgroundPosition: coords });
            });
        });
    }
    
    function isMobile() {
        return $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
    }

}); // end ready
